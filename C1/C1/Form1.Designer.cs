﻿namespace C1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.outputDisplay = new System.Windows.Forms.PictureBox();
            this.multiCommand = new System.Windows.Forms.RichTextBox();
            this.errorBox = new System.Windows.Forms.RichTextBox();
            this.singleCommand = new System.Windows.Forms.RichTextBox();
            this.runBtn = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.syntaxbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.outputDisplay)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // outputDisplay
            // 
            this.outputDisplay.BackColor = System.Drawing.Color.White;
            this.outputDisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.outputDisplay.Location = new System.Drawing.Point(3, 31);
            this.outputDisplay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.outputDisplay.Name = "outputDisplay";
            this.outputDisplay.Size = new System.Drawing.Size(725, 387);
            this.outputDisplay.TabIndex = 0;
            this.outputDisplay.TabStop = false;
            this.outputDisplay.Paint += new System.Windows.Forms.PaintEventHandler(this.outputDisplay_Paint);
            // 
            // multiCommand
            // 
            this.multiCommand.BackColor = System.Drawing.Color.Linen;
            this.multiCommand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiCommand.ForeColor = System.Drawing.SystemColors.Desktop;
            this.multiCommand.Location = new System.Drawing.Point(733, 31);
            this.multiCommand.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.multiCommand.Name = "multiCommand";
            this.multiCommand.Size = new System.Drawing.Size(363, 432);
            this.multiCommand.TabIndex = 1;
            this.multiCommand.Text = "";
            // 
            // errorBox
            // 
            this.errorBox.BackColor = System.Drawing.Color.SteelBlue;
            this.errorBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.errorBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorBox.ForeColor = System.Drawing.SystemColors.Desktop;
            this.errorBox.Location = new System.Drawing.Point(3, 423);
            this.errorBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.errorBox.Name = "errorBox";
            this.errorBox.ReadOnly = true;
            this.errorBox.Size = new System.Drawing.Size(725, 162);
            this.errorBox.TabIndex = 1;
            this.errorBox.Text = "";
            // 
            // singleCommand
            // 
            this.singleCommand.BackColor = System.Drawing.Color.Azure;
            this.singleCommand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.singleCommand.ForeColor = System.Drawing.SystemColors.Desktop;
            this.singleCommand.Location = new System.Drawing.Point(733, 469);
            this.singleCommand.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.singleCommand.Multiline = false;
            this.singleCommand.Name = "singleCommand";
            this.singleCommand.Size = new System.Drawing.Size(363, 40);
            this.singleCommand.TabIndex = 1;
            this.singleCommand.Text = "";
            this.singleCommand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.singleCommand_KeyDown);
            // 
            // runBtn
            // 
            this.runBtn.BackColor = System.Drawing.Color.Crimson;
            this.runBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runBtn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.runBtn.Location = new System.Drawing.Point(733, 514);
            this.runBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.runBtn.Name = "runBtn";
            this.runBtn.Size = new System.Drawing.Size(152, 70);
            this.runBtn.TabIndex = 2;
            this.runBtn.Text = "Run";
            this.runBtn.UseVisualStyleBackColor = false;
            this.runBtn.Click += new System.EventHandler(this.runBtn_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1101, 28);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(125, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(125, 26);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(125, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(133, 26);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // syntaxbtn
            // 
            this.syntaxbtn.BackColor = System.Drawing.Color.Crimson;
            this.syntaxbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.syntaxbtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.syntaxbtn.Location = new System.Drawing.Point(891, 514);
            this.syntaxbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.syntaxbtn.Name = "syntaxbtn";
            this.syntaxbtn.Size = new System.Drawing.Size(207, 70);
            this.syntaxbtn.TabIndex = 4;
            this.syntaxbtn.Text = "Error Check";
            this.syntaxbtn.UseVisualStyleBackColor = false;
            this.syntaxbtn.Click += new System.EventHandler(this.syntaxbtn_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.ClientSize = new System.Drawing.Size(1101, 587);
            this.Controls.Add(this.syntaxbtn);
            this.Controls.Add(this.runBtn);
            this.Controls.Add(this.singleCommand);
            this.Controls.Add(this.errorBox);
            this.Controls.Add(this.multiCommand);
            this.Controls.Add(this.outputDisplay);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "C1";
            ((System.ComponentModel.ISupportInitialize)(this.outputDisplay)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox outputDisplay;
        private System.Windows.Forms.RichTextBox multiCommand;
        private System.Windows.Forms.RichTextBox errorBox;
        private System.Windows.Forms.RichTextBox singleCommand;
        private System.Windows.Forms.Button runBtn;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button syntaxbtn;
    }
}

