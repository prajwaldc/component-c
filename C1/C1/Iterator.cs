﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1
{
    public class Iterator : IAbstractIterator
    {
        private ErrorCollection _collection;
        private int _current = 0;
        private int _step = 1;

        // Constructor

        public Iterator(ErrorCollection collection)
        {
            this._collection = collection;
        }

        // Gets first item

        public Item First()
        {
            _current = 0;
            return _collection[_current] as Item;
        }

        // Gets next item

        public Item Next()
        {
            _current += _step;
            if (!IsDone)
                return _collection[_current] as Item;
            else

                return null;
        }

        // Gets or sets stepsize

        public int Step
        {
            get { return _step; }
            set { _step = value; }
        }

        // Gets current iterator item

        public Item CurrentItem
        {
            get { return _collection[_current] as Item; }
        }

        // Gets whether iteration is com


        public bool IsDone
        {
            get { return _current >= _collection.Count; }
        }
    }



    /// <summary>

    /// A collection item

    /// </summary>

    public class Item

    {
        private string _name;

        // Constructor

        public Item(string name)
        {
            this._name = name;
        }

        // Gets name

        public string Name
        {
            get { return _name; }
        }
    }

    /// <summary>

    /// The 'Aggregate' interface

    /// </summary>
    interface IAbstractCollection

    {
        Iterator CreateIterator();
    }

    /// <summary>

    /// The 'Iterator' interface

    /// </summary>

    interface IAbstractIterator

    {
        Item First();
        Item Next();
        bool IsDone { get; }
        Item CurrentItem { get; }
    }

    /// <summary>

    /// The 'ConcreteAggregate' class

    /// </summary>

    public class ErrorCollection : IAbstractCollection

    {
        private ArrayList _items = new ArrayList();

        public Iterator CreateIterator()
        {
            return new Iterator(this);
        }

        // Gets item count

        public int Count
        {
            get { return _items.Count; }
        }

        // Indexer

        public object this[int index]
        {
            get { return _items[index]; }
            set { _items.Add(value); }
        }

        public void Append(Item item)
        {
            _items.Add(item);
        }

        public void Clear()
        {
            _items.Clear();
        }
    }
}
