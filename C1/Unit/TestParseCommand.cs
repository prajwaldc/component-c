﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using C1;

namespace Unit
{
    [TestClass]
    public class TestParseCommand
    {
        /// <summary>
        /// Tests if command
        /// </summary>
        [TestMethod]
        public void If_test()
        {
            Canvas myCanvas = new Canvas();
            Parse parser = new Parse();
            RichTextBox rtb = new RichTextBox();

            rtb.Text = "if 100>50 \r\n" +
                "moveto 100,200 \r\n" +
                "endif";

            parser.parseCommand(rtb, myCanvas, new ErrorCollection());

            int expectedX = 100;
            int expectedY = 200;
            int actualX = myCanvas.x;
            int actualY = myCanvas.y;

            Assert.AreEqual(expectedX, actualX, "IF command Test Failed!");
            Assert.AreEqual(expectedY, actualY, "IF command Test Failed!");
        }

        /// <summary>
        /// Tests while command 
        /// </summary>
        [TestMethod]
        public void While_test()
        {
            Canvas myCanvas = new Canvas();
            Parse parser = new Parse();
            RichTextBox rtb = new RichTextBox();

            rtb.Text = "a=10 \r\n" +
                "loop a<40 \r\n" +
                "moveto a,a \r\n" +
                "a=a+10 \r\n" +
                "endloop";

            parser.parseCommand(rtb, myCanvas, new ErrorCollection());

            int expectedX = 30;
            int expectedY = 30;
            int actualX = myCanvas.x;
            int actualY = myCanvas.y;

            Assert.AreEqual(expectedX, actualX, "WHILE command Test Failed!");
            Assert.AreEqual(expectedY, actualY, "WHILE command Test Failed!");
        }

        /// <summary>
        /// Tests variable assignment 
        /// </summary>
        [TestMethod]
        public void Variable_test()
        {
            Canvas myCanvas = new Canvas();
            Parse parser = new Parse();
            RichTextBox rtb = new RichTextBox();
            Dictionary<String, String> variables;

            rtb.Text = "a=10 \r\n";

            parser.parseCommand(rtb, myCanvas, new ErrorCollection());

            variables = parser.variables;

            String expectedA = "10";
            String actualA = variables["a"];

            Assert.AreEqual(expectedA, actualA, "Variable assignment Test Failed!");
        }

        /// <summary>
        /// Tests method declaration and call
        /// </summary>
        [TestMethod]
        public void Method_test()
        {
            Canvas myCanvas = new Canvas();
            Parse parser = new Parse();
            RichTextBox rtb = new RichTextBox();

            rtb.Text = "method mymethod(a,b) \r\n" +
                "moveto a,b \r\n" +
                "endmethod \r\n" +
                "mymethod(100,200) \r\n";

            parser.parseCommand(rtb, myCanvas, new ErrorCollection());

            int expectedX = 100;
            int expectedY = 200;
            int actualX = myCanvas.x;
            int actualY = myCanvas.y;

            Assert.AreEqual(expectedX, actualX, "Method command Test Failed!");
            Assert.AreEqual(expectedY, actualY, "Method command Test Failed!");
        }

    }
}
