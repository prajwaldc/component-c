﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using C1;

namespace Unit
{
    //test methods of parse class
    [TestClass]
    public class ParseTest
    {
        //tests if parse line successfully updates the canvas object from given instructions
        [TestMethod]
        public void TestParseDrawTo()
        {
            //   parseLine(String line, Canvas displayCanvas, StringBuilder errorList, int numLine)

            Canvas myCanvas = new Canvas();
            Parse parser = new Parse();

            //parser.parseLine(new StringBuilder(), myCanvas, "drawto 200,100",  1);
            parser.parseLine(new ErrorCollection(), myCanvas, "drawto 200,100", 1);

            int[] expectedValues = { 200, 100 };
            int[] obtainedValues = { myCanvas.x, myCanvas.y };

            //run test
            Assert.AreEqual(expectedValues[0], obtainedValues[0], "Test Failed!");
            Assert.AreEqual(expectedValues[1], obtainedValues[1], "Test Failed!");


        }
        //tests if parse line successfully updates the canvas object from given instructions
        [TestMethod]
        public void TestParseMoveTo()
        {
            //   parseLine(String line, Canvas displayCanvas, StringBuilder errorList, int numLine)

            Canvas myCanvas = new Canvas();
            Parse parser = new Parse();

            parser.parseLine(new ErrorCollection(), myCanvas, "moveto 200,100", 1);

            int[] expectedValues = { 200, 100 };
            int[] obtainedValues = { myCanvas.x, myCanvas.y };

            //run test
            Assert.AreEqual(expectedValues[0], obtainedValues[0], "Test Failed!");
            Assert.AreEqual(expectedValues[1], obtainedValues[1], "Test Failed!");


        }

    }
}
